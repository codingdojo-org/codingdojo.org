---
title: "WhatIsCodingDojo"
draft: false
date: "2024-09-19T22:52:00"
aliases:
  - "/fr/WhatIsCodingDojo"

---

Definition
----------

Un Coding Dojo réunit un groupe de codeuses et codeurs 
pour travailler ensemble sur un défi de programmation. 
Cette assemblée est là pour s’amuser 
et s’engager dans une [Pratique délibérée](/DeliberatePractice) 
afin d’améliorer leurs compétences.

Le [ParisDojo](/dojo/ParisDojo) s’attache à coder devant les autres, 
le plus souvent depuis zéro, 
dans un temps très court (1h à 1h30).
Ils utilisent différents langages, outils et formats d’exercices. 
Ils considèrent qu’un exercice est réussi
lorsqu’il est terminé dans le temps imparti
ET quand chacun peut répéter l’exercice chez lui.

Les [principes du Coding Dojo](/CodingDojoPrinciples) 
peuvent aider à comprendre 
ce qu’est le [CodingDojo](/fr/CodingDojo).

Principe
--------

- L’acquisition de compétences en matière de codage 
  devrait être un processus continu ;

Caractéristiques
----------------

- Un environnement 
  non-compétitif,
  collaboratif,
  et amusant ;
- Tous les niveaux de compétences sont les bienvenus ;
- Possibilité d’essayer des nouvelles idées en toute sécurité ;

Pré-requis
----------

- Une salle de réunion avec suffisament de places assises
  (le nombre de participants varie généralement entre 5 et 20 ?) ;
- Au moins un PC ou un ordinateur portable ;
- Un projecteur numérique ('beamer') ;

Déroulé
-------

- version du [ParisDojo](/dojo/ParisDojo) :
  - **2 minutes :** 
    décider d’une date pour la prochaine session,
  - **25-30 minutes :** 
    rétrospective rapide de la session précédente
    ce qui s’est bien passé, 
    ce qui était intéressant, 
    ce qui était frustrant,
  - **10 minutes :**
    décider d’un sujet pour cette session 
    (nous appelons ces trois premiers points le protocole “next, prev, this”),
  - **40 minutes environ :**
    coder ! 
    un [kata préparé](/PreparedKata) 
    ou un [RandoriKata](/RandoriKata),
    voir ci-dessous,
  - **5-10 minutes :**
    pause de mi-session 
    pour discuter de l’évolution de la situation,
  - **40 minutes :**
    continuer à coder ;

Types de réunions
-----------------

### [Kata préparé](/PreparedKata)

- Un-e présentateur-ice montre 
  comment résoudre le défi
  à partir de zéro, 
  en utilisant [TDD](/TestDrivenDevelopment) 
  et [la technique des petits pas](/BabySteps) ;
- Chaque étape doit avoir du sens
  pour toutes les personnes présentes ;
- Les participants ne doivent interrompre 
  que s’ils ne comprennent pas ce qui se passe ;

### [RandoriKata](/RandoriKata)

- Le défi est résolu par un binôme de codeur-euses
  (une personne qui tappe au clavier : “driver” 
  et une personne qui donne une intention : “co-pilote”) ;
- Toutes les personnes présentes sont invitées à aider ;
- Chaque binôme dispose d’un petit délai (5 or 7 minutes) pour avancer, 
  en utilisant [TDD](/TestDrivenDevelopment) 
  et [la technique des petits pas](/BabySteps) ;
- À la fin du temps imparti, 
  la personne “driver” retourne dans le public, 
  la personne “co-pilote” devient “driver” 
  et une personne du public devient “co-pilote” ;

