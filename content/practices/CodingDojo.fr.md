---
title: "CodingDojo"
draft: false
date: "2024-09-19T23:07:21"
aliases:
  - "/fr/CodingDojo"

---

Si je veux apprendre le judo, 
je vais m’inscrire au dojo du coin 
et y passer une heure par semaine pendant deux ans, 
au bout de quoi j'aurai peut-être envie 
de pratiquer plus assidûment. 
Des années d'entraînement supplémentaire 
peuvent être récompensées par l’obtention d’une ceinture noire, 
qui n’est que le signe d’une ascension 
vers une autre étape de l'apprentissage. 
Aucun maître ne cesse d'apprendre. 

Si je veux apprendre la programmation objet…
mon employeur va me trouver une formation de trois jours à Java 
tiré de l’édition de cette année du catalogue 
d’une grande société de formation. 
L’acquisition de compétences en matière de codage 
n’est pas un processus de gratification instantanée.

--
[LaurentBossavit](/people/LaurentBossavit)
